def convert_unicode_to_ascii(unicode_str):
  return unicode_str.encode('utf-8')


def get_insert_query(entry_dict, table_name):
    columns = ', '.join(entry_dict.keys())
    values = tuple(entry_dict.values())
    insert_query = "INSERT INTO {} ({}) VALUES {}".format(table_name, columns, values)
    return insert_query


def get_update_query(entry_dict, table_name, where_cnd=None):
    set_list = ["{} = {}".format(k, v) for k,v in entry_dict]
    set_clause = ", ".join(set_list)
    if where_cnd is None or len(where_cnd) == 0:
        update_query = "UPDATE {} SET {}".format(table_name, set_clause)
    else:
        update_query = "UPDATE {} SET {} WHERE {}".format(table_name, set_clause, where_cnd)
    return update_query
    

def get_delete_query(table_name, where_cnd=None):
    if where_cnd is None or len(where_cnd) == 0:
        delete_query = "DELETE FROM {}".format(table_name)
    else:
        delete_query = "DELETE FROM {} WHERE {}".format(table_name, where_cnd)
    return delete_query